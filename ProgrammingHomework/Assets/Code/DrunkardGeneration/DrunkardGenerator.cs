﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;
using System;
using UnityEngine.UI;

public class DrunkardGenerator : MonoBehaviour
{
    [Header("PRNG")]
    [SerializeField]
    [Tooltip("If set to true, the seed below will be used. Else it uses the default seed")]
    private bool _useSeed;
    [SerializeField]
    [Tooltip("Seed to use for the generation")]
    private int _seed;
    [SerializeField]
    [Tooltip("Reference to the gameboard which displays the result of the drunkard walk")]
    private GameBoard _gameBoard;
    [SerializeField]
    [Tooltip("Number of generations used to smooth the PCG result")]
    private int _generations;
    private Random _rng;
    private Vector2Int _curPosition = Vector2Int.zero;
    [SerializeField][Tooltip("Amount of tiles that need to be converted")][Range(1,10000)]
    private int _selectedTileRange;
    private bool _finished;
    [Tooltip("Amount of tiles that were there at the beginning")]
    private int _maxTiles;
    [Tooltip("Amount of tiles that are left")]
    private int _remainingTiles;
    [SerializeField]
    [Range(1, 1000)][Tooltip("")]
    private float _timeOfGeneration;
    [SerializeField]
    private float _speedOfGeneration;
    [SerializeField]
    private SpriteRenderer[,] _newTile;
    [SerializeField]
    private SpriteRenderer _tile;

    private void Start()
    {
        _remainingTiles = _maxTiles;
        Generate();       
        _maxTiles = _gameBoard.Height * _gameBoard.Width;

    }
    private void Update()
    {
        _curPosition.x = Mathf.Clamp(_curPosition.x, 3, _gameBoard.Width - 3);

        _curPosition.y = Mathf.Clamp(_curPosition.y, 3, _gameBoard.Height - 3);

        Vector2 pos = _curPosition;
        transform.position = pos;      
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            other.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
    
     
    void Generate()
    {
        StartCoroutine(DrunkWalk());
    }
    IEnumerator DrunkWalk()
    {
        
        if (_remainingTiles <= _maxTiles - _selectedTileRange)
        {
            StopCoroutine(DrunkWalk());
        }
        Debug.Log("Hello");
        _rng = _useSeed ? new Random(_seed) : new Random();

        _curPosition = new Vector2Int(_rng.Next(5, _gameBoard.Width), _rng.Next(5, _gameBoard.Height));
        Vector2Int _direction = new Vector2Int(0, 0);
        _speedOfGeneration = Time.time;
        _finished = true;
        while (_finished)
        {
            _remainingTiles = _maxTiles - 1;

            var dir = _rng.Next(0, 4);

            if (dir == 0)
            {
                _curPosition.y++;

            }
            else if (dir == 1)
            {
                _curPosition.y--;

            }
            else if (dir == 2)
            {
                _curPosition.x--;
    
            }
            else if (dir == 3)
            {
                _curPosition.x++;
       
            }
            _generations++;
            _speedOfGeneration += 1f / _timeOfGeneration;
            if (_speedOfGeneration > Time.time)
            {
                yield return new WaitUntil(() => _speedOfGeneration < Time.time);
            }
        }
    }
}
