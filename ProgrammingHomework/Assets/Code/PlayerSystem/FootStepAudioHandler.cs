﻿using System;
using UEGP3.Core;
using UnityEngine;

namespace UEGP3.PlayerSystem
{
    [RequireComponent(typeof(AudioSource))]
    public class FootStepAudioHandler : MonoBehaviour
    {
        [Tooltip("The audiosource used to play the audioclips that are referenced in the animation")]
        private AudioSource _audioSource;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        // Called as an animation event
        // NOTE REGARDING HOMEWORK 2: Because we gave the AnimationEvent an object to reference, we can use this method and give it the object as a parameter to use it
        private void DoFootStepSound(ScriptableAudioEvent scriptableEvent)
        {
            scriptableEvent.Play(_audioSource);
        }
    }
}